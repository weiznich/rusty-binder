//! Utilities for testing a Compiler implementation.

use compiler::syntax::ast;

pub fn parse_ty_from_str(source: &str) -> ast::Ty {
    let sess = ::syntax::parse::ParseSess::new();
    let mut parser = ::syntax::parse::new_parser_from_source_str(
        &sess,
        vec![],
        "".into(),
        source.into(),
    );

    let ty = parser.parse_ty();
    match ty {
        Ok(p) => (*p).clone(),
        _ => panic!("internal testing error: could not parse type from {:?}", source),
    }
}

/// Generate a binding containing a single file with no dependencies.
pub fn binding_from_file_contents<P>(language: &str, path: P, contents: &str) -> ::Binding
where ::std::path::PathBuf: ::std::convert::From<P>,
{
    ::Binding {
        language: language.into(),
        files: vec![
            super::File {
                path: ::std::path::PathBuf::from(path),
                contents: contents.into(),
            },
        ],
        dependencies: vec![],
    }
}

/// Compare the file contents of two bindings using the given closure.
///
/// The first argument of the closure will be the expected contents while the second will be the
/// actual contents. The closure should panic if the file contents compare unequal.
pub fn cmp_bindings_with<F>(expected: &::Binding, actual: &::Binding, func: &F)
where F: Fn(&str, &str),
{
    assert!(expected.language == actual.language, "bindings are for different languages");

    for (expected_file, actual_file) in expected.files.iter().zip(&actual.files) {
        assert!(expected_file.path == actual_file.path, "file paths do not match");
        func(&expected_file.contents, &actual_file.contents);
    }

    for (expected_dep, actual_dep) in expected.dependencies.iter().zip(&actual.dependencies) {
        cmp_bindings_with(expected_dep, actual_dep, func);
    }
}

//! Relevant re-exports from `syntex_syntax`.
//!
//! If you think that there are other relevant types which should be exported here, please open an
//! issue on the [repo](https://gitlab.com/rusty-binder/rusty-binder).

pub mod abi {
    pub use syntax::abi::Abi;
}

pub mod ast {
    pub use syntax::ast::Attribute;
    pub use syntax::ast::BareFnTy;
    pub use syntax::ast::FnDecl;
    pub use syntax::ast::FunctionRetTy;
    pub use syntax::ast::Item;
    pub use syntax::ast::Mutability;
    pub use syntax::ast::MutTy;
    pub use syntax::ast::Path;
    pub use syntax::ast::Ty;
    pub use syntax::ast::TyKind;
}

pub mod codemap {
    pub use syntax::codemap::BytePos;
    pub use syntax::codemap::Span;
    pub use syntax::codemap::spanned;
}

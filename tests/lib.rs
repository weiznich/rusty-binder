extern crate binder;

#[test]
#[should_panic(expected = "has not been brought into global scope")]
fn module_not_pub_used() {
    let mut binder = binder::Binder::new().expect("internal testing error: io error reading manifest");
    binder.source_string("
        mod c {
            pub type Byte = i8;
        }
    ");
    binder.module("c").expect("internal testing error: malformed module path");

    if let Err(..) = binder.compile() {
        // should panic here
        panic!("compilation errors: {}", binder.errors());
    }
}

#[test]
#[should_panic(expected = "malformed module path")]
fn malformed_module_path() {
    let mut binder = binder::Binder::new().expect("internal testing error: io error reading manifest");
    binder.module("c:api").expect("malformed module path");  // should panic here

    if let Err(..) = binder.compile() {
        panic!("compilation errors: {}", binder.errors());
    }
}

#[test]
#[should_panic(expected = "could not be found")]
fn module_not_found() {
    let mut binder = binder::Binder::new().expect("internal testing error: io error reading manifest");
    binder.source_string("
        pub use c::*;
        mod c {
            pub type Byte = i8;
        }
    ");
    binder.module("c::api").expect("internal testing error: malformed module path");

    if let Err(..) = binder.compile() {
        // should panic here
        panic!("compilation errors: {}", binder.errors());
    }
}
